import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'alarm_model.g.dart';

@JsonSerializable()
class Alarm extends Equatable {
  final int id;
  final String time;
  final bool isSwitched;

  Alarm({this.time, this.id, this.isSwitched});

  factory Alarm.fromJson(Map<String, dynamic> json) => _$AlarmFromJson(json);
  Map<String, dynamic> toJson() => _$AlarmToJson(this);

  @override
  List<Object> get props => [id, time, isSwitched];
}
