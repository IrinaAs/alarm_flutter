enum DaysOfWeek {
  Never,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
  Sunday
}

class DaysOfWeekModel {
  DaysOfWeek week;
  bool flagChoice;
  Map<DaysOfWeek, bool> mapWeek;
}
