import 'package:alarmclock/model/day_of_week_model.dart';
import 'package:equatable/equatable.dart';

abstract class SettingTimeEvent extends Equatable {
  const SettingTimeEvent();
}

class StartSettingTimeEvent extends SettingTimeEvent {
  @override
  String toString() => "StartSettingTimeEvent";
  List<Object> get props => [];
}

class GetTimeEvent extends SettingTimeEvent {
  final String time;

  GetTimeEvent(this.time);

  @override
  String toString() => "GetTimeEvent {$time}";
  List<Object> get props => [time];
}

// class CancelDatePickerSettingTimeEvent extends SettingTimeEvent {
//   @override
//   String toString() => "CancelDatePickerSettingTimeEvent";
//   List<Object> get props => [];
// }

class TransitionToScreenSettingRepeatEvent extends SettingTimeEvent {
  @override
  String toString() => "TransitionToScreenSettingRepeatEvent";
  List<Object> get props => [];
}

class TransitionToScreenChoiceSingEvent extends SettingTimeEvent {
  @override
  String toString() => "TransitionToScreenChoiceSingEvent";
  List<Object> get props => [];
}

class RepeatSignal extends SettingTimeEvent {
  @override
  String toString() => "TransitionToScreenChoiceSingEvent";
  List<Object> get props => [];
}

class TransitionToAlarmVaultTimeEvent extends SettingTimeEvent {
  @override
  String toString() => "TransitionToAlarmVaultTimeEvent";
  List<Object> get props => [];
}

class PushBtnDoneInTextFieldEvent extends SettingTimeEvent {
  final String title;
  final String time;

  PushBtnDoneInTextFieldEvent(this.title, this.time);

  @override
  String toString() => "PushBtnDoneTextFieldEvent {$title, $time}";
  List<Object> get props => [title, time];
}

class PushBtnCancelInAlertDialogEvent extends SettingTimeEvent {
  @override
  String toString() => "PushBtnCancelInAlertDialogEvent";
  List<Object> get props => [];
}

//Event для simpleDialog
class ChoiceDayOfWeekEvent extends SettingTimeEvent {
  final DaysOfWeekModel modelDay;
  ChoiceDayOfWeekEvent(this.modelDay);
  @override
  String toString() => "ChoiceDayOfWeekEvent {$modelDay}";
  List<Object> get props => [];
}
