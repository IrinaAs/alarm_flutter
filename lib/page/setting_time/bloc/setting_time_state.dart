import 'package:alarmclock/model/day_of_week_model.dart';
import 'package:equatable/equatable.dart';

abstract class SettingTimeState extends Equatable {
  const SettingTimeState();
}

class SettingTimeInitialState extends SettingTimeState {
  final String timeInit = "00:00";
  final String titleInit = "Будильник";
  @override
  String toString() =>
      "SettingTimeInitialState {timeInit: $timeInit, titleInit: $titleInit}";
  List<Object> get props => [timeInit, titleInit];
}

class TimeEstablishedAndClosedPickerState extends SettingTimeState {
  final String time;

  TimeEstablishedAndClosedPickerState(this.time);

  @override
  String toString() => "TimeEstablishedAndClosedPickerState {time: $time}";
  List<Object> get props => [time];
}

class NavigateNextPageRepeatByDayState extends SettingTimeState {
  final String routeName;

  NavigateNextPageRepeatByDayState(this.routeName);

  @override
  String toString() =>
      "NavigateNextPageRepeatByDayState {routeName: $routeName}";

  @override
  List<Object> get props => [routeName];
}

class NavigateNextPageChoiceSingState extends SettingTimeState {
  final String routeName;

  NavigateNextPageChoiceSingState(this.routeName);

  @override
  String toString() =>
      "NavigateNextPageChoiceSingState {routeName: $routeName}";

  @override
  List<Object> get props => [routeName];
}

class SettedRepeatSigmalState extends SettingTimeState {
  @override
  String toString() => "SettedRepeatSigmalState";

  @override
  List<Object> get props => [];
}

class NavigateBackPageState extends SettingTimeState {
  final String routeName;

  NavigateBackPageState(this.routeName);

  @override
  String toString() => "NavigateBackPageState {routeName: $routeName}";
  List<Object> get props => [routeName];
}

class CloseDatePickerState extends SettingTimeState {
  @override
  String toString() => "CloseDatePickerState";
  List<Object> get props => [];
}

class TitleEstablishedAndDidBtnDoneAlertDialogState extends SettingTimeState {
  final String routeName;

  TitleEstablishedAndDidBtnDoneAlertDialogState(this.routeName);
  @override
  String toString() =>
      "TitleEstablishedAndDidBtnDoneAlertDialogState {routeName: $routeName}";

  @override
  List<Object> get props => [routeName];
}

//State для alertDialog
class DidBtnCancelAlertDialogState extends SettingTimeState {
  @override
  String toString() => "DidBtnCancelAlertDialogState";
  List<Object> get props => [];
}

class DidBtnDoneInTextFieldState extends SettingTimeState {
  final String time;
  final String title;

  DidBtnDoneInTextFieldState(this.title, this.time);
  @override
  String toString() =>
      "DidBtnDoneAlertDialogState {title: $title, time: $time}}";
  List<Object> get props => [title, time];
}

//State для simpleDialog
class DidBtnDoneSimpleDialogState extends SettingTimeState {
  @override
  String toString() => "DidBtnDoneSimpleDialogState";
  List<Object> get props => [];
}

class ChoicedDayOfWeekState extends SettingTimeState {
  final DaysOfWeekModel modelDay;
  ChoicedDayOfWeekState(this.modelDay);
  @override
  String toString() => "ChoicedDayOfWeekState {$modelDay}";
  List<Object> get props => [modelDay];
}

class SettingDayOfWeekInTileLiewState extends SettingTimeState {
  final DaysOfWeekModel modelDay;
  SettingDayOfWeekInTileLiewState(this.modelDay);

  @override
  String toString() => "SettingDayOfWeekInTileLiewState {$modelDay}";
  List<Object> get props => [modelDay];
}

// class DidBtnCancelSimpleDialogState extends SettingTimeState {
//   @override
//   String toString() => "TapedBtnDoneState";
//   List<Object> get props => [];
// }
