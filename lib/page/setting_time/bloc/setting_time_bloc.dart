import 'dart:async';
// import 'package:alarmclock/model/alarm_model.dart';
import 'package:alarmclock/model/day_of_week_model.dart';
import 'package:alarmclock/page/alarm_vault/alarm_vault_page.dart';
import 'package:alarmclock/page/choice_sing/choice_sing_page.dart';
import 'package:bloc/bloc.dart';
import 'bloc.dart';

abstract class SettingTimeBlocType {
  // void onClosePickerClicked() {}
  void onDonePickerClicked({String time});
  void onBtnEntryClicked();
  void onClickedBtnCancelInAlertDialog();
  void onClickedBtnDoneInTextField({String text});
  void onClickedChoiceDayOfWeekInSimpleDialog({DaysOfWeekModel model});
}

class SettingTimeBloc extends Bloc<SettingTimeEvent, SettingTimeState>
    implements SettingTimeBlocType {
  @override
  SettingTimeState get initialState => SettingTimeInitialState();

  @override
  void onTransition(Transition<SettingTimeEvent, SettingTimeState> transition) {
    print(transition);
  }

  // void onClosePickerClicked() {
  //   add(CancelDatePickerSettingTimeEvent());
  // }

  void onDonePickerClicked({String time}) {
    add(GetTimeEvent(time));
  }

  void onBtnEntryClicked() {
    add(TransitionToAlarmVaultTimeEvent());
  }

  // add alertDialog
  void onClickedBtnCancelInAlertDialog() {
    add(PushBtnCancelInAlertDialogEvent());
  }

  void onClickedBtnDoneInTextField({String text, String time}) {
    add(PushBtnDoneInTextFieldEvent(text, time));
  }

  //add simpleDialog
  void onClickedChoiceDayOfWeekInSimpleDialog({DaysOfWeekModel model}) {
    add(ChoiceDayOfWeekEvent(model));
  }

  @override
  Stream<SettingTimeState> mapEventToState(SettingTimeEvent event) async* {
    if (event is StartSettingTimeEvent) {
      yield SettingTimeInitialState();

      //CommonScreen
    } else if (event is GetTimeEvent) {
      yield TimeEstablishedAndClosedPickerState(event.time);

      // }
      // else if (event is CancelDatePickerSettingTimeEvent) {
      //   yield CloseDatePickerState();

      //
    } else if (event is TransitionToAlarmVaultTimeEvent) {
      yield NavigateBackPageState(AlarmVaultPage.routeName);
    } else if (event is TransitionToScreenSettingRepeatEvent) {
      // yield NavigateNextPageRepeatByDayState(TitleAlarmPage.routeName);

      //simpleDialog
    } else if (event is ChoiceDayOfWeekEvent) {
      DaysOfWeekModel _model =
          _handleChoiceOptionsInSimpleDialog(model: event.modelDay);
      yield ChoicedDayOfWeekState(_model);
    } else if (event is DidBtnDoneSimpleDialogState) {
      yield SettingDayOfWeekInTileLiewState(DaysOfWeekModel());
    }

    //alertdialog
    else if (event is PushBtnCancelInAlertDialogEvent) {
      yield DidBtnCancelAlertDialogState();
    } else if (event is PushBtnDoneInTextFieldEvent) {
      yield DidBtnDoneInTextFieldState(event.title, event.time);

      //
    } else if (event is TransitionToScreenChoiceSingEvent) {
      yield NavigateNextPageChoiceSingState(ChoiceSingPage.routeName);
    } else if (event is RepeatSignal) {
      yield SettedRepeatSigmalState();
    }
  }

  DaysOfWeekModel _handleChoiceOptionsInSimpleDialog({DaysOfWeekModel model}) {
    if (model.mapWeek == null) {
      model.mapWeek[model.week] = true;
    } else if (model.mapWeek[model.week] == model.week) {
      model.mapWeek[model.week] = false;
    } else if (model.mapWeek.keys != model.week) {
      model.mapWeek[model.week] = true;
    }
    return model;
  }
}

// switch (model.week) {
//       case DaysOfWeek.Never:
//         print("Никогда");

//         break;
//       case DaysOfWeek.Monday:
//         print("Понедельник");

//         break;
//       case DaysOfWeek.Tuesday:
//         print("Вторник");

//         break;
//       case DaysOfWeek.Wednesday:
//         print("Среда");

//         break;
//       case DaysOfWeek.Thursday:
//         print("Четверг");

//         break;
//       case DaysOfWeek.Friday:
//         print("Пятница");

//         break;
//       case DaysOfWeek.Saturday:
//         print("Суббота");

//         break;
//       case DaysOfWeek.Sunday:
//         print("Воскресенье");

//         break;
//     }

// abstract class SettingTimePresenterType {
//   handleChoiceTimeInPicker({DateTime time});
//   addAlarmDB();
// }

// class SettingTimePresenter extends SettingTimePresenterType {
// String hour = "00";
// String minute = "00";

// handleChoiceTimeInPicker({DateTime time}) {
//   String formattedTimeHour = DateFormat('kk').format(time);
//   String formattedTimeMinute = DateFormat('mm').format(time);
//   hour = formattedTimeHour;
//   minute = formattedTimeMinute;
// }

// addAlarmDB() {
//   Alarm objectAlarm = Alarm(time: '$hour : $minute');
//   DBProvider.db.newAlarm(objectAlarm);
// }
// }
