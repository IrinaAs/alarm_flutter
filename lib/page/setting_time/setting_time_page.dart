import 'package:alarmclock/helpers/repeat_alarm_simple_dialog.dart';
import 'package:alarmclock/helpers/time_picker.dart';
import 'package:alarmclock/helpers/title_alarm_alert.dart';
import 'package:alarmclock/page/setting_time/bloc/setting_time_bloc.dart';
import 'package:alarmclock/page/setting_time/bloc/setting_time_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum DaysOfWeek {
  Never,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
  Sunday
}

class SettingTimePage extends StatefulWidget {
  static const routeName = '/settingTime';

  @override
  _SettingTimePageState createState() => _SettingTimePageState();
}

class _SettingTimePageState extends State<SettingTimePage> {
  bool _flag = false;

  void initState() {
    super.initState();
  }

  Widget _buildTitleSetAlarm({String title}) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(fontSize: 18),
        ));
  }

  Widget _buildPickerChoiceTime({String time}) {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 140,
          width: 160,
          child: RaisedButton(
              onPressed: () {
                TimePicker().showDatePicker(context);
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              color: Color.fromRGBO(176, 59, 70, 10),
              child: Text(
                time,
                style: TextStyle(fontSize: 35, color: Colors.white),
              )),
        ),
      ],
    ));
  }

  Widget _buildSettingListView({String title}) {
    final _bloc = BlocProvider.of<SettingTimeBloc>(context);
    return Expanded(
        child: Container(
            margin: EdgeInsets.only(top: 50),
            child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            border: Border(
                                top: BorderSide(
                                  color: Color.fromRGBO(199, 199, 199, 100),
                                  width: 0.3,
                                ),
                                bottom: BorderSide(
                                  color: Color.fromRGBO(199, 199, 199, 100),
                                  width: 0.3,
                                ))),
                        child: GestureDetector(
                          onTap: () async {
                            showDialog(
                                context: context,
                                builder: (_) {
                                  return SimpleDialogPage();
                                });
                          },
                          child: ListTile(
                              title: Row(
                            children: <Widget>[
                              Text(
                                "Повтор",
                                style: TextStyle(fontSize: 18),
                              ),
                              Spacer(),
                              Text(
                                "Никогда",
                                style: TextStyle(fontSize: 18),
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: Color.fromRGBO(176, 59, 70, 10),
                                size: 20,
                              )
                            ],
                          )),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          AlertDialogPage()
                              .buildAlertDialog(context)
                              .then((onValue) {
                            SnackBar bar = SnackBar(
                                content: Text("Изменено название на $onValue"));
                            Scaffold.of(context).showSnackBar(bar);
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(
                                    color: Color.fromRGBO(199, 199, 199, 100),
                                    width: 0.3,
                                  ),
                                  bottom: BorderSide(
                                    color: Color.fromRGBO(199, 199, 199, 100),
                                    width: 0.3,
                                  ))),
                          child: ListTile(
                              title: Row(
                            children: <Widget>[
                              Text(
                                "Название",
                                style: TextStyle(fontSize: 18),
                              ),
                              Spacer(),
                              Text(
                                title,
                                style: TextStyle(fontSize: 18),
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: Color.fromRGBO(176, 59, 70, 10),
                                size: 20,
                              )
                            ],
                          )),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => print("3"),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(
                                    color: Color.fromRGBO(199, 199, 199, 100),
                                    width: 0.3,
                                  ),
                                  bottom: BorderSide(
                                    color: Color.fromRGBO(199, 199, 199, 100),
                                    width: 0.3,
                                  ))),
                          child: ListTile(
                              title: Row(children: <Widget>[
                            Text(
                              "Мелодия",
                              style: TextStyle(fontSize: 18),
                            ),
                            Spacer(),
                            Text(
                              "Радар",
                              style: TextStyle(fontSize: 18),
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              color: Color.fromRGBO(176, 59, 70, 10),
                              size: 20,
                            ),
                          ])),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => print("4"),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(
                                    color: Color.fromRGBO(199, 199, 199, 100),
                                    width: 0.3,
                                  ),
                                  bottom: BorderSide(
                                    color: Color.fromRGBO(199, 199, 199, 100),
                                    width: 0.3,
                                  ))),
                          child: ListTile(
                              title: Row(
                            children: <Widget>[
                              Text(
                                "Повторение сигнала",
                                style: TextStyle(fontSize: 18),
                              ),
                              Spacer(),
                              Transform.scale(
                                scale: 0.9,
                                child: Switch.adaptive(
                                  key: UniqueKey(),
                                  value: true,
                                  onChanged: (bool value) {},
                                  activeColor: Color.fromRGBO(176, 59, 70, 10),
                                ),
                              )
                            ],
                          )),
                        ),
                      ),
                    ],
                  )
                ])));
  }

  Widget _buildButtonAddAlarm() {
    final _bloc = BlocProvider.of<SettingTimeBloc>(context);
    return Container(
      width: double.infinity,
      child: RaisedButton(
        onPressed: () => _bloc.onBtnEntryClicked(),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(4.0))),
        color: Color.fromRGBO(176, 59, 70, 10),
        child: Text(
          "Создать",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget _complexWidgetsOnScreen({String time, String title}) {
    return Container(
        margin: EdgeInsets.only(top: 30, left: 15, right: 15, bottom: 40),
        child: Column(children: <Widget>[
          _buildTitleSetAlarm(title: title),
          _buildPickerChoiceTime(time: time),
          _buildSettingListView(title: title),
          _buildButtonAddAlarm()
        ]));
  }

  Widget _buildInitState({String time, String title}) {
    return Stack(children: <Widget>[
      Container(
        child: _complexWidgetsOnScreen(time: time, title: title),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
      )
    ]);
  }

  Widget _handleBlocState() {
    Widget _widget;
    return BlocBuilder<SettingTimeBloc, SettingTimeState>(
        builder: (context, state) {
      if (state is SettingTimeInitialState) {
        _widget = _buildInitState(time: state.timeInit, title: state.titleInit);
        //
      } else if (state is TimeEstablishedAndClosedPickerState) {
        _widget = _buildInitState(time: state.time, title: "Будильник");
      } else if (state is DidBtnCancelAlertDialogState) {
        // }
        // else if (state is CloseDatePickerState) {
        //
      } else if (state is DidBtnDoneInTextFieldState) {
        _widget = _buildInitState(time: state.time, title: state.title);
        //
      } else if (state is NavigateBackPageState) {
        Future.delayed(Duration(milliseconds: 40), () {
          Navigator.of(context).pushReplacementNamed(state.routeName);
        });
      }
      return _widget;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: SizedBox(
              height: MediaQuery.of(context).size.height - kToolbarHeight,
              child: _handleBlocState(),
            )));
  }
}
