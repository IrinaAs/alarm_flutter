import 'package:alarmclock/model/alarm_model.dart';
import 'package:equatable/equatable.dart';

abstract class AlarmVaultState extends Equatable {
  const AlarmVaultState();
}

class AlarmVaultEmptyState extends AlarmVaultState {
  @override
  String toString() => "AlarmVaultEmptyState";

  @override
  List<Object> get props => [];
}

class AlarmVaultLoadingState extends AlarmVaultState {
  @override
  String toString() => "AlarmVaultLoadingState";

  @override
  List<Object> get props => [];
}

class AlarmVaultLoadedState extends AlarmVaultState {
  final List<Alarm> data;

  AlarmVaultLoadedState(this.data);

  @override
  String toString() => "AlarmVaultLoadedState{data: $data}";

  @override
  List<Object> get props => [data];
}

class NavigateNextPageState extends AlarmVaultState {
  final String routeName;

  NavigateNextPageState(this.routeName);

  @override
  String toString() => "NavigateNextPageState {routeName: $routeName}";

  @override
  List<Object> get props => [routeName];
}

class AlarmVaultErrorState extends AlarmVaultState {
  final String error;

  AlarmVaultErrorState(this.error);

  @override
  String toString() => "AlarmVaultErrorState {error: $error}";

  @override
  List<Object> get props => [error];
}
