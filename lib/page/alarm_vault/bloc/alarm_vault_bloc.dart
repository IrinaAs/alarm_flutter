import 'dart:async';
import 'package:alarmclock/model/alarm_model.dart';
import 'package:alarmclock/page/setting_time/setting_time_page.dart';
import 'package:alarmclock/repository/alarm_repository.dart';
import 'package:bloc/bloc.dart';
import 'bloc.dart';

abstract class AlarmVaultBlocType {
  void startedApp();
  void onBtnAddAlarmClicked();
  void onSwipeDelete();
}

class AlarmVaultBloc extends Bloc<AlarmVaultEvent, AlarmVaultState>
    implements AlarmVaultBlocType {
  final _repository = AlarmRepository();
  final _alarm = Alarm();

  @override
  AlarmVaultState get initialState => AlarmVaultEmptyState();

  @override
  void onTransition(Transition<AlarmVaultEvent, AlarmVaultState> transition) {
    print(transition);
  }

  void startedApp() {
    add(AppStartedEvent());
  }

  void onBtnAddAlarmClicked() {
    add(TransitionToSettingTimeAlarmEvent());
  }

  void onSwipeDelete() {
    add(DeleteAlarmEvent());
  }

  @override
  Stream<AlarmVaultState> mapEventToState(AlarmVaultEvent event) async* {
    List<Alarm> data;
    if (event is AppStartedEvent) {
      try {
        data = await _repository.getAllAlarms();
        yield AlarmVaultLoadedState(data);
      } catch (error) {
        yield AlarmVaultLoadedState(error);
      }
    } else if (event is DeleteAlarmEvent) {
      data = await _repository.deleteAlarmById(_alarm.id);
      yield AlarmVaultLoadedState(data);
    } else if (event is DeleteAllAlarmsEvent) {
      data = await _repository.deleteAllAlarms();
      yield AlarmVaultLoadedState(data);
    } else if (event is TransitionToSettingTimeAlarmEvent) {
      yield NavigateNextPageState(SettingTimePage.routeName);
    }
  }
}

// abstract class AlarmVaultPresenterType {
//   Future<List<Alarm>> getAllAlarms();
//   deleteAlarmItemFromTable(Alarm item);
//   deleteAllAlarmFromTable();
// }

// class AlarmVaultPresenter extends AlarmVaultPresenterType {
//   List<String> items = [];

//   Future<List<Alarm>> getAllAlarms() {
//     return DBProvider.db.getAlarms();
//   }

//   deleteAlarmItemFromTable(Alarm item) {
//     DBProvider.db.updateAlarms(item);
//     DBProvider.db.deleteAlarm(item.id);
//     print("Item: $item");
//   }

//   deleteAllAlarmFromTable() {
//     DBProvider.db.deleteAllAlarms();
//   }
// }
