import 'package:equatable/equatable.dart';

abstract class AlarmVaultEvent extends Equatable {
  const AlarmVaultEvent();
}

class AppStartedEvent extends AlarmVaultEvent {
  @override
  String toString() => "AppStartedEvent";
  List<Object> get props => [];
}

class DeleteAlarmEvent extends AlarmVaultEvent {
  @override
  String toString() => "DeletedAlarmEvent";
  List<Object> get props => [];
}

class DeleteAllAlarmsEvent extends AlarmVaultEvent {
  @override
  String toString() => "DeleteAllAlarmsEvent";
  List<Object> get props => [];
}

class TransitionToSettingTimeAlarmEvent extends AlarmVaultEvent {
  @override
  String toString() => "TransitionedToSettingTimeAlarmEvent";
  List<Object> get props => [];
}
