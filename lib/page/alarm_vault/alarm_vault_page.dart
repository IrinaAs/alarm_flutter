import 'package:alarmclock/model/alarm_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/bloc.dart';

class AlarmVaultPage extends StatefulWidget {
  static const routeName = '/alarmVault';

  @override
  _AlarmVaultPageState createState() => _AlarmVaultPageState();
}

class _AlarmVaultPageState extends State<AlarmVaultPage> {
  final AlarmVaultBloc _bloc = AlarmVaultBloc();

  void initState() {
    super.initState();
  }

  Widget _buildSegmentButton() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child:
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Expanded(
            child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                bottomLeft: Radius.circular(20.0)),
          ),
          color: Color.fromRGBO(68, 68, 68, 20),
          child: Text("Schedule"),
          textColor: Colors.white,
          onPressed: () {},
        )),
        SizedBox(width: 2),
        Expanded(
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0)),
            ),
            color: Color.fromRGBO(68, 68, 68, 20),
            child: Text(
              "Meeting",
            ),
            textColor: Colors.white,
            onPressed: () {},
          ),
        )
      ]),
    );
  }

  Widget _buildListView() {
    return Expanded(
        child: Container(
            margin: EdgeInsets.only(top: 20), child: _handleListViewState()));
  }

  Widget _buildEmptyListTile() {
    return Container(
        child: Center(
            child: Text(
      "Нет будильников",
      style: TextStyle(color: Color.fromRGBO(199, 199, 199, 40), fontSize: 16),
    )));
  }

  Widget _buildAddAlarm({List<Alarm> alarms}) {
    return ListView.builder(
        itemCount: alarms.length,
        itemBuilder: (context, index) {
          return Dismissible(
              crossAxisEndOffset: 2,
              key: UniqueKey(),
              direction: DismissDirection.endToStart,
              background: Container(
                padding: EdgeInsets.only(right: 8.0),
                alignment: Alignment.centerRight,
                color: Color.fromRGBO(176, 59, 70, 10),
                child: Text("Удалить", style: TextStyle(color: Colors.white)),
              ),
              onDismissed: (direction) {},
              child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                          top: BorderSide(
                            color: Color.fromRGBO(199, 199, 199, 100),
                            width: 0.3,
                          ),
                          bottom: BorderSide(
                            color: Color.fromRGBO(199, 199, 199, 100),
                            width: 0.3,
                          ))),
                  child: ListTile(
                      contentPadding: EdgeInsets.all(0),
                      title: Container(
                        alignment: Alignment.centerLeft,
                        child: Row(children: <Widget>[
                          Icon(
                            Icons.alarm,
                            color: alarms[index].isSwitched == true
                                ? Color.fromRGBO(176, 59, 70, 10)
                                : Color.fromRGBO(170, 170, 170, 1),
                            size: 30.0,
                          ),
                          SizedBox(width: 45),
                          Expanded(
                            child: Column(children: [
                              Text(alarms[index].time,
                                  style: TextStyle(
                                      fontSize: 32,
                                      color: Color.fromRGBO(68, 68, 68, 20))),
                              Text(
                                "<Заметки>",
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Color.fromRGBO(156, 156, 156, 100)),
                              ),
                            ]),
                          ),
                          Transform.scale(
                              scale: 0.8,
                              child: Container(
                                  alignment: Alignment.centerRight,
                                  child: Switch.adaptive(
                                    key: UniqueKey(),
                                    value: alarms[index].isSwitched,
                                    onChanged: (bool value) {},
                                    activeColor:
                                        Color.fromRGBO(176, 59, 70, 10),
                                  )))
                        ]),
                      ))));
        });
  }

  Widget _buildOutputError({String error}) {
    return Text(error);
  }

  Widget _buildProgressIndicator() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _handleListViewState() {
    return BlocBuilder<AlarmVaultBloc, AlarmVaultState>(
        builder: (context, state) {
      if (state is AlarmVaultEmptyState) {
        _bloc.startedApp();
        return _buildEmptyListTile();
      } else if (state is AlarmVaultLoadingState) {
        return _buildProgressIndicator();
      } else if (state is AlarmVaultLoadedState) {
        return _buildAddAlarm(alarms: state.data);
      } else if (state is AlarmVaultErrorState) {
        return _buildOutputError(error: state.error);
      } else if (state is NavigateNextPageState) {
        Future.delayed(Duration(milliseconds: 40), () {
          Navigator.of(context).pushReplacementNamed(state.routeName);
        });
      }
      return Container();
    });
  }

  Widget _buildAddButton({Alarm input}) {
    final AlarmVaultBloc _alarmVaultBloc =
        BlocProvider.of<AlarmVaultBloc>(context);
    return Container(
      width: double.infinity,
      child: RaisedButton(
        onPressed: () {
          _alarmVaultBloc.onBtnAddAlarmClicked();
        },
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(4.0))),
        color: Color.fromRGBO(176, 59, 70, 10),
        child: Text(
          "Добавить",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget _buildSeparate() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        color: Color.fromRGBO(199, 199, 199, 100),
      ),
      child: SizedBox(
        height: 3,
      ),
      alignment: Alignment.center,
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      centerTitle: true,
      actions: <Widget>[
        Container(
          margin: EdgeInsets.only(right: 16.0),
          child: GestureDetector(
            child: Icon(Icons.delete, color: Color.fromRGBO(176, 59, 70, 10)),
            onTap: () {
              _bloc.onSwipeDelete();
            },
          ),
        ),
      ],
      title:
          Text('Wake up!', style: TextStyle(color: Colors.black, fontSize: 18)),
      elevation: 0,
    );
  }

  Widget _buildBody({Alarm data}) {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 20),
      child: Column(
        children: <Widget>[
          _buildSeparate(),
          _buildSegmentButton(),
          _buildListView(),
          _buildAddButton(input: data)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _data = ModalRoute.of(context).settings.arguments as Alarm;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: _buildAppBar(),
        body: _buildBody(data: _data));
  }
}
