import 'package:alarmclock/helpers/repeat_alarm_simple_dialog.dart';
import 'package:alarmclock/page/alarm_vault/alarm_vault_page.dart';
import 'package:alarmclock/page/alarm_vault/bloc/alarm_vault_bloc.dart';
import 'package:alarmclock/page/setting_time/bloc/setting_time_bloc.dart';
import 'package:alarmclock/page/setting_time/bloc/setting_time_state.dart';
import 'package:alarmclock/page/setting_time/setting_time_page.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(Main());

class Main extends StatelessWidget {
  // final channel = ChannelHelper();

  @override
  Widget build(BuildContext context) {
    // channel.callNativeMethod();
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: MaterialApp(
            title: 'Alarm',
            theme: ThemeData(fontFamily: "Menlo"),
            debugShowCheckedModeBanner: false,
            initialRoute: AlarmVaultPage.routeName,
            // home: BlocBuilder<SettingTimeBloc, SettingTimeState>(
            //     builder: (context, state) {
            //   if (state is ChoicedDayOfWeekState) {
            //     return SimpleDialogPage();
            //   }
            // }),
            routes: {
              AlarmVaultPage.routeName: (ctx) => BlocProvider<AlarmVaultBloc>(
                    create: (ctx) => AlarmVaultBloc(),
                    child: AlarmVaultPage(),
                  ),
              SettingTimePage.routeName: (ctx) => BlocProvider<SettingTimeBloc>(
                    create: (ctx) => SettingTimeBloc(),
                    child: SettingTimePage(),
                  ),
            }));
  }
}
