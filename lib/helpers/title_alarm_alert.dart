import 'package:alarmclock/page/setting_time/bloc/setting_time_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class AlertDialogType {
  Future<String> buildAlertDialog(BuildContext context);
}

class AlertDialogPage extends AlertDialogType {
  static final String initialText = 'Будильник';
  TextEditingController _controller = TextEditingController(text: initialText);

  Future<String> buildAlertDialog(BuildContext context) async {
    final _bloc = BlocProvider.of<SettingTimeBloc>(context);
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Row(
              children: <Widget>[
                Text('Название'),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.alarm,
                  color: Color.fromRGBO(176, 59, 70, 10),
                ),
              ],
            ),
            content: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    autofocus: true,
                    onChanged: (value) {
                      // _bloc.onClickedBtnDoneInTextField(text: value);
                      // Navigator.of(context).pop(_controller.text.toString());
                    },
                    controller: _controller,
                    cursorColor: Color.fromRGBO(176, 59, 70, 10),
                    decoration: InputDecoration(
                        suffixIcon: IconButton(
                            icon:
                                Icon(Icons.clear, color: Colors.grey, size: 15),
                            onPressed: () {
                              _controller.text = '';
                              _controller.clear();
                            }),
                        helperText: "Введите название"),
                  ),
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text(
                    'Отмена',
                    style: TextStyle(color: Colors.black),
                  ),
                  onPressed: () {
                    // _bloc.onClickedBtnCancelInAlertDialog();
                    Navigator.of(context).pop();
                  }),
            ],
          );
        });
  }
}
