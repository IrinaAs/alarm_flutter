import 'package:alarmclock/page/setting_time/bloc/setting_time_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class TimePickerType {
  void showDatePicker(context);
}

class TimePicker extends TimePickerType {
  int hour = 0;
  int min = 0;

  void showDatePicker(context) {
    final _bloc = BlocProvider.of<SettingTimeBloc>(context);
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 200.0,
            color: Colors.white,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CupertinoButton(
                  child: Text("Отмена",
                      style: TextStyle(color: Colors.black.withOpacity(0.4))),
                  onPressed: () {
                    // _bloc.onClosePickerClicked();
                    Navigator.of(context).pop();
                  },
                ),
                Expanded(
                  child: CupertinoPicker(
                      scrollController: FixedExtentScrollController(
                        initialItem: 0,
                      ),
                      itemExtent: 32.0,
                      backgroundColor: Colors.white,
                      onSelectedItemChanged: (int index) {
                        hour = index;
                      },
                      children: List<Widget>.generate(24, (int index) {
                        return Center(
                          child: Text(setTime(index)),
                        );
                      })),
                ),
                Center(child: Text(":")),
                Expanded(
                  child: CupertinoPicker(
                      scrollController: FixedExtentScrollController(
                        initialItem: 0,
                      ),
                      itemExtent: 32.0,
                      backgroundColor: Colors.white,
                      onSelectedItemChanged: (int index) {
                        min = index;
                      },
                      children: List<Widget>.generate(60, (int index) {
                        return Center(
                          child: Text(setTime(index)),
                        );
                      })),
                ),
                FlatButton(
                  child: Text("Готово", style: TextStyle(color: Colors.black)),
                  onPressed: () {
                    _bloc.onDonePickerClicked(
                        time: "${setTime(hour)}:${setTime(min)}");
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  String setTime(int index) {
    if (index < 10) {
      return "0$index";
    } else {
      return "$index";
    }
  }
}
