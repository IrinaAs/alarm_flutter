import 'package:alarmclock/model/day_of_week_model.dart';
import 'package:alarmclock/page/setting_time/bloc/setting_time_bloc.dart';
import 'package:alarmclock/page/setting_time/bloc/setting_time_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SimpleDialogPage extends StatefulWidget {
  @override
  _SimpleDialogPageState createState() => _SimpleDialogPageState();
}

class _SimpleDialogPageState extends State<SimpleDialogPage> {
  Widget _buildSimpleDialog() {
    return SimpleDialog(
      title: Center(
        child: Text('Повтор',
            style: TextStyle(color: Color.fromRGBO(176, 59, 70, 10))),
      ),
      children: _buildOptions(),
    );
  }

  List<Widget> _buildOptions() {
    final _bloc = BlocProvider.of<SettingTimeBloc>(context);
    DaysOfWeekModel _model = DaysOfWeekModel();
    return <Widget>[
      SimpleDialogOption(
          child: _buildCardOption(text: "Никогда"),
          onPressed: () {
            _model.week = DaysOfWeek.Never;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      SimpleDialogOption(
          child: _buildCardOption(text: "Понедельник"),
          onPressed: () {
            _model.week = DaysOfWeek.Monday;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      SimpleDialogOption(
          child: _buildCardOption(text: "Вторник"),
          onPressed: () {
            _model.week = DaysOfWeek.Tuesday;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      SimpleDialogOption(
          child: _buildCardOption(text: "Среда"),
          onPressed: () {
            _model.week = DaysOfWeek.Wednesday;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      SimpleDialogOption(
          child: _buildCardOption(text: "Четверг"),
          onPressed: () {
            _model.week = DaysOfWeek.Thursday;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      SimpleDialogOption(
          child: _buildCardOption(text: "Пятница"),
          onPressed: () {
            _model.week = DaysOfWeek.Friday;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      SimpleDialogOption(
          child: _buildCardOption(text: "Суббота"),
          onPressed: () {
            _model.week = DaysOfWeek.Saturday;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      SimpleDialogOption(
          child: _buildCardOption(text: "Воскресение"),
          onPressed: () {
            _model.week = DaysOfWeek.Sunday;
            _bloc.onClickedChoiceDayOfWeekInSimpleDialog(model: _model);
          }),
      _buildButtonDoneAndCancel()
    ];
  }

  Widget _buildCardOption({String text}) {
    return Card(
        elevation: 0.6,
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Row(children: <Widget>[
            Text(text),
            Spacer(),
            _buildIconeChoiceOption()
          ]),
        ));
  }

  Widget _buildButtonDoneAndCancel() {
    return Row(
      children: <Widget>[
        FlatButton(
          child: Text("Oтмена", style: TextStyle(color: Colors.black)),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        Spacer(),
        FlatButton(
          child: Text("Готово", style: TextStyle(color: Colors.black)),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }

  Widget _buildIconeChoiceOption() {
    return true
        ? Icon(Icons.check, color: Color.fromRGBO(176, 59, 70, 10), size: 15)
        : Text("");
  }

  Widget _handleBlocState() {
    Widget _widget;
    return BlocBuilder<SettingTimeBloc, SettingTimeState>(
        bloc: BlocProvider.of<SettingTimeBloc>(context),
        builder: (context, state) {
          if (state is ChoicedDayOfWeekState) {
            _widget = _buildSimpleDialog();
            return _widget;
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return _handleBlocState();
  }
}
