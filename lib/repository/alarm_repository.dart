import 'package:alarmclock/model/alarm_model.dart';
import 'package:alarmclock/repository/alarm_dao.dart';

class AlarmRepository {
  final alarmDao = AlarmDao();

  Future getAllAlarms() => alarmDao.getAlarms();

  Future insertAlarms(Alarm alarm) => alarmDao.newAlarm(alarm);

  Future updateAlarms(Alarm alarm) => alarmDao.updateAlarms(alarm);

  Future deleteAlarmById(int id) => alarmDao.deleteAlarm(id);

  Future deleteAllAlarms() => alarmDao.deleteAllAlarms();
}
