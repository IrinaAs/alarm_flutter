import 'package:alarmclock/model/alarm_model.dart';
import 'package:alarmclock/repository/database.dart';

class AlarmDao {
  final dbProvider = DBProvider.db;

  Future<int> newAlarm(Alarm newAlarm) async {
    final db = await dbProvider.database;
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM alarms");
    int id = table.first["id"];
    var raw = await db.rawInsert(
        "INSERT Into alarms (id, time)"
        " VALUES (?,?)",
        [id, newAlarm.time]);
    print("id: $id");
    return raw;
  }

  Future<int> updateAlarms(Alarm newAlarm) async {
    final db = await dbProvider.database;
    var res = await db.update("alarms", newAlarm.toJson(),
        where: "id = ?", whereArgs: [newAlarm.id]);
    return res;
  }

  Future<List<Alarm>> getAlarms() async {
    final db = await dbProvider.database;
    List<Map> list = await db.rawQuery('SELECT * FROM alarms');
    List<Alarm> alarms = new List();
    for (int i = 0; i < list.length; i++) {
      alarms.add(new Alarm(id: list[i]["id"], time: list[i]["time"]));
    }
    print(alarms);
    return alarms;
  }

  Future<int> deleteAlarm(int id) async {
    final db = await dbProvider.database;
    int res = await db.delete("alarms", where: "id = ?", whereArgs: [id]);
    return res;
  }

  Future<int> deleteAllAlarms() async {
    final db = await dbProvider.database;
    int res = await db.delete('alarms');
    return res;
  }
}
